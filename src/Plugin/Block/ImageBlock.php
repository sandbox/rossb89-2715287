<?php

/**
 * @file
 * Contains \Drupal\image_blocks\Plugin\Block\ImageBlock.
 */

namespace Drupal\image_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\image_blocks\FlickrApi;
use Drupal\image_blocks\InstagramApi;

/**
 * Provides an 'ImageBlock' block
 *
 * @Block(
 *   id = "image_blocks_imageblock",
 *   admin_label = @Translation("Image Block"),
 * )
 */
class ImageBlock extends BlockBase implements BlockPluginInterface {

  const FLICKR_RESPONSE_FAIL = 'fail';
  const FLICKR_RESPONSE_SUCCESS = 'ok';

  /**
   * {@inheritdoc}
   */
  public function build() {

    $block_config = $this->getConfiguration();
    $module_config = \Drupal::config('image_blocks.settings');

    // Number of images
    $images_config['number_of_images'] = $block_config['image_block_number_of_images'];

    // Account Name
    if (empty($block_config['image_block_account_name'])) {
      // Try use the default from the module config
      $images_config['account_name'] = $module_config->get($block_config['image_block_images_source'] . '_default_user_account');
    }
    else {
      $images_config['account_name'] = $block_config['image_block_account_name'];
    }

    $image_data = [];

    switch ($block_config['image_block_images_source']) {
      case 'flickr':
        $images_config['api_key'] = $module_config->get('flickr_api_key');
        $image_data = $this->get_images_from_flickr($images_config);
        break;
      case 'instagram':
        $images_config['api_key'] = $module_config->get('instagram_access_token');
        $image_data = $this->get_images_from_instagram($images_config);
        break;
    }

    // See if we have overridden the max-age cache time, or set the default to 3 hours
    $cachetime = $block_config['image_block_cache_lifetime'] !== '' ? $block_config['image_block_cache_lifetime'] : 10800;

    return array(
      'image_block' => array(
        '#theme' => array('image_blocks_block__' . $block_config['image_block_images_source'], 'image_blocks_block'),
        '#image_data' => $image_data,
        '#attached' => [
          'library' => [
            'image_blocks/image-blocks'
          ]
        ],
        '#cache' => [
          'max-age' => $cachetime,
        ],
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $block_config = $this->getConfiguration();
    $module_config = \Drupal::config('image_blocks.settings');
    $renderer = \Drupal::service('renderer');

    $enabled_providers = $module_config->get('enabled_providers');
    $providers = [];
    foreach ($enabled_providers as $provider => $status) {
      if ($status === 1) {
        $providers[$provider] = $this->t(ucwords($provider));
      }
    }
    
    $url = Link::fromTextAndUrl($this->t('module configuration page'), Url::fromRoute('image_blocks.config_form'))->toRenderable();

    $form['image_block_images_source'] = array(
      '#type' => 'select',
      '#title' => $this->t('Images source'),
      '#description' => $this->t('Choose the source of the images. No providers will be shown here unless enabled on the @url.', array('@url' => $renderer->render($url))),
      '#default_value' => isset($block_config['image_block_images_source']) ? $block_config['image_block_images_source'] : NULL,
      '#required' => TRUE,
      '#options' => $providers,
    );

    $form['image_block_account_name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Account name'),
      '#description' => $this->t('Account name to pull in images from. This will override the defaults (if set) on the @url. <br/> Leave blank to use the default configured account name.', array('@url' => $renderer->render($url))),
      '#default_value' => isset($block_config['image_block_account_name']) ? $block_config['image_block_account_name'] : NULL,
    );

    $form['image_block_number_of_images'] = array(
      '#type' => 'number',
      '#title' => $this->t('Number of images'),
      '#description' => $this->t('The maximum number of images to display in this block.'),
      '#default_value' => isset($block_config['image_block_number_of_images']) ? $block_config['image_block_number_of_images'] : 1,
      '#min' => 0,
      '#max' => 100 // @todo - what would a sensible default be?
    );

    $form['image_block_cache_lifetime'] = array(
      '#type' => 'number',
      '#title' => $this->t('Cache lifetime override'),
      '#description' => $this->t('<p>The max-age cache lifetime of this block in seconds. If set, this will override the default block cache setting of 3 hours.</p>
To set the cache for this block to never expire (until such time the cache is cleared manually), set to <strong>-1</strong>. <br/>
To disabling caching for this block, set to <strong>0</strong>.<br/>
<strong>Setting to 0 is not recommended as you could potentially cause a *lot* of API calls and may violate the provider\'s terms and conditions if you go over the set limit(s)</strong>.'),
      '#default_value' => isset($block_config['image_block_cache_lifetime']) ? $block_config['image_block_cache_lifetime'] : NULL,
      '#min' => -1,
    );
    
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['image_block_images_source'] = $form_state->getValue('image_block_images_source');
    $this->configuration['image_block_number_of_images'] = $form_state->getValue('image_block_number_of_images');
    $this->configuration['image_block_account_name'] = $form_state->getValue('image_block_account_name');
    $this->configuration['image_block_cache_lifetime'] = $form_state->getValue('image_block_cache_lifetime');

    parent::blockSubmit($form, $form_state);
  }

  /**
   * Attempts to pull in images from Flickr
   *
   * @param array $images_config -
   *  Config array including no. images, account to pull from, etc.
   *
   * @return array $images
   */
  protected function get_images_from_flickr($images_config) {

    $flickr = new FlickrApi($images_config['api_key']);

    // Get the NSID from the username (screen name)
    $params = [
      'method' => 'flickr.people.findByUsername',
      'username' => $images_config['account_name'],
    ];

    $username_response = $flickr->apiCall($params);

    if (!$username_response || $username_response && $username_response['stat'] == self::FLICKR_RESPONSE_FAIL) {
      \Drupal::logger('image_blocks')->notice(t('Unable to load flicker account information for username %user', array('%user' => $images_config['account_name'])));
      return [];
    }
    elseif ($username_response['stat'] == self::FLICKR_RESPONSE_SUCCESS) {
      $username_id = $username_response['user']['nsid'];
    }

    $params = [
      'method' => 'flickr.photos.search',
      'content_type' => '1',
      'extras' => 'url_l,url_sq',
      'media' => 'photos',
      'per_page' => isset($images_config['number_of_images']) ? $images_config['number_of_images'] : '1',
      'user_id' => $username_id,
    ];

    $images_response = $flickr->apiCall($params);

    $images = [];

    if (isset($images_response['photos']['photo'])) {
      foreach ($images_response['photos']['photo'] as $delta => $image_data) {
        $params = [
          'method' => 'flickr.photos.getInfo',
          'photo_id' => $image_data['id'],
        ];

        $image = $flickr->apiCall($params);

        $images[] = [
          'url_thumbnail' => isset($image_data['url_sq']) ? $image_data['url_sq'] : '',
          'height_thumbnail' => isset($image_data['height_sq']) ? $image_data['height_sq'] : '',
          'width_thumbnail' => isset($image_data['width_sq']) ? $image_data['width_sq'] : '',
          'url_large' => isset($image_data['url_l']) ? $image_data['url_l'] : '',
          'height_large' => isset($image_data['height_l']) ? $image_data['height_l'] : '',
          'width_large' => isset($image_data['width_l']) ? $image_data['width_l'] : '',
          'title' => isset($image['photo']['title']['_content']) ? $image['photo']['title']['_content'] : '',
          'description' => isset($image['photo']['description']['_content']) ? $image['photo']['description']['_content'] : '',
          'image_page_url' => isset($image['photo']['urls']['url'][0]['_content']) ? $image['photo']['urls']['url'][0]['_content'] : '',
        ];
      }
    }

    return $images;
  }

  /**
   * Attempts to pull in images from Instagram.
   *
   * @param array $images_config -
   *  Config array including no. images, account to pull from, etc.
   *
   * @return array $images
   */
  protected function get_images_from_instagram($images_config) {

    $instagram = new InstagramApi($images_config['api_key']);

    // Get the user ID from the username
    $endpoint = 'users/search';
    $params = [
      'q' => $images_config['account_name'],
    ];

    $username_response = $instagram->apiCall($endpoint, $params);

    // Let's just double check we found the correct user
    if ($username_response) {
      foreach ($username_response['data'] as $user) {
        if ($user['username'] == $images_config['account_name']) {
          $user_id = $user['id'];
        }
      }
    }
    if (!isset($user_id) || !$username_response) {
      \Drupal::logger('image_blocks')->notice(t('Unable to load instagram account information for username %user', array('%user' => $images_config['account_name'])));
      return [];
    }

    $endpoint = 'users/' . $user_id . '/media/recent';
    $params = [
      'count' => isset($images_config['number_of_images']) ? $images_config['number_of_images'] : '1',
    ];

    $media_response = $instagram->apiCall($endpoint, $params);

    $images = [];

    if ($media_response && !empty($media_response['data'])) {
      foreach ($media_response['data'] as $media) {
        if ($media['type'] != 'image') {
          continue;
        }
        $images[] = [
          'url_thumbnail' => isset($media['images']['thumbnail']['url']) ? $media['images']['thumbnail']['url'] : '',
          'height_thumbnail' => isset($media['images']['thumbnail']['height']) ? $media['images']['thumbnail']['height'] : '',
          'width_thumbnail' => isset($media['images']['thumbnail']['width']) ? $media['images']['thumbnail']['width'] : '',
          'url_large' => isset($media['images']['standard_resolution']['url']) ? $media['images']['standard_resolution']['url'] : '',
          'height_large' => isset($media['images']['standard_resolution']['height']) ? $media['images']['standard_resolution']['height'] : '',
          'width_large' => isset($media['images']['standard_resolution']['width']) ? $media['images']['standard_resolution']['width'] : '',
          'title' => isset($media['caption']['text']) ? $media['caption']['text'] : '',
          'description' => isset($media['caption']['text']) ? $media['caption']['text'] : '',
          'image_page_url' => isset($media['link']) ? $media['link'] : '',
        ];
      }
    }

    return $images;
  }

}