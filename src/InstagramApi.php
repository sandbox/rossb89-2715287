<?php

/**
 * @file
 * Contains \Drupal\image_blocks\InstagramApi.
 */

namespace Drupal\image_blocks;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class InstagramApi {
  private $accessToken = '';

  const INSTAGRAM_API_BASE_URL = 'https://api.instagram.com/v1/';

  public function __construct($accessToken) {
    $this->accessToken = $accessToken;
  }

  public function apiCall($endpoint, $params) {
    $params['access_token'] = $this->accessToken;

    $encoded_params = [];

    foreach ($params as $k => $v) {
      $encoded_params[] = urlencode($k) . '=' . urlencode($v);
    }

    $url = self::INSTAGRAM_API_BASE_URL . $endpoint . '?' . implode('&', $encoded_params);

    $client = new Client();
    try {
      $response = $client->get($url);
      $response_object = json_decode($response->getBody(), TRUE);
      if (empty($response_object)) {
        return FALSE;
      }
    }
    catch (RequestException $e) {
      // @todo - log a notice?
      return FALSE;
    }

    return $response_object;
  }
  
}