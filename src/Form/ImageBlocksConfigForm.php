<?php

/**
 * @file
 * Contains \Drupal\image_blocks\Form\ImageBlocksConfigForm.
 */

namespace Drupal\image_blocks\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Custom settings form for the image_blocks module
 */
class ImageBlocksConfigForm extends ConfigFormBase {

  /**
   * Constructor for ImageBlocksConfigForm.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'image_blocks_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['image_blocks.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $image_blocks_config = $this->config('image_blocks.settings');

    if (\Drupal::moduleHandler()->moduleExists('help')) {
      $help_url = Link::fromTextAndUrl($this->t('module help page'), Url::fromRoute('help.page', array('name' => 'image_blocks')))->toRenderable();
    }

    $renderer = \Drupal::service('renderer');

    $form['image_blocks']['api_keys'] = array(
      '#type' => 'details',
      '#title' => $this->t('API Keys'),
    );

    $form['image_blocks']['api_keys']['flickr'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Flickr')
    );

    $form['image_blocks']['api_keys']['flickr']['flickr_api_key'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Flickr API Key'),
      '#default_value' => $image_blocks_config->get('flickr_api_key') ? $image_blocks_config->get('flickr_api_key') : '',
    );

    if (!empty($help_url)) {
      $form['image_blocks']['api_keys']['flickr']['flickr_api_key']['#description'] = $this->t('For information on how to get a Flickr API key, visit the @help_url.', array('@help_url' => $renderer->render($help_url)));
    }

    $form['image_blocks']['api_keys']['instagram'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Instagram')
    );

    $form['image_blocks']['api_keys']['instagram']['instagram_access_token'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Instagram Access Token'),
      '#default_value' => $image_blocks_config->get('instagram_access_token') ? $image_blocks_config->get('instagram_access_token') : '',
    );

    if (!empty($help_url)) {
      $form['image_blocks']['api_keys']['instagram']['instagram_access_token']['#description'] = $this->t('For information on how to get an Instagram Access token, visit the @help_url.', array('@help_url' => $renderer->render($help_url)));
    }

    // Swap the integer value back to the array key for the #default_value
    $enabled_providers = $image_blocks_config->get('enabled_providers');
    foreach ($enabled_providers as $provider => $enabled) {
      if ($enabled === 1) {
        $enabled_providers[$provider] = $provider;
      }
    }

    $form['image_blocks']['defaults'] = array(
      '#type' => 'details',
      '#title' => $this->t('Default configuration'),
    );

    $form['image_blocks']['defaults']['flickr'] = array(
      '#type' => 'details',
      '#title' => $this->t('Flickr'),
    );

    $form['image_blocks']['defaults']['flickr']['flickr_default_user_account'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Flickr - Default user account (screen name)'),
      '#description' => $this->t('The default account name (screen name) of which to pull images from.'),
      '#default_value' => $image_blocks_config->get('flickr_default_user_account') ? $image_blocks_config->get('flickr_default_user_account') : '',
    );

    $form['image_blocks']['defaults']['instagram'] = array(
      '#type' => 'details',
      '#title' => $this->t('Instagram'),
    );

    $form['image_blocks']['defaults']['instagram']['instagram_default_user_account'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Instagram - Default user account'),
      '#description' => $this->t('The default account name of which to pull images from.'),
      '#default_value' => $image_blocks_config->get('instagram_default_user_account') ? $image_blocks_config->get('instagram_default_user_account') : '',
    );

    $form['image_blocks']['providers'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Enabled providers'),
    );

    $form['image_blocks']['providers']['enabled_providers'] = array(
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled providers'),
      '#description' => $this->t('Warning - Disabling providers here that already have blocks created may cause the blocks to stop functioning correctly.'),
      '#default_value' => $enabled_providers ? $enabled_providers : [NULL, NULL],
      '#options' => ['flickr' => $this->t('Flickr'), 'instagram' => $this->t('Instagram')]
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Let's change the values from the label to an integer, if enabled
    $enabled_providers = $form_state->getValue('enabled_providers');

    foreach ($enabled_providers as $provider => $enabled) {
      if ($enabled !== 0) {
        $enabled_providers[$provider] = 1;
      }
    }

    $this->configFactory()->getEditable('image_blocks.settings')
      ->set('flickr_api_key', $form_state->getValue('flickr_api_key'))
      ->set('instagram_access_token', $form_state->getValue('instagram_access_token'))
      ->set('flickr_default_user_account', $form_state->getValue('flickr_default_user_account'))
      ->set('instagram_default_user_account', $form_state->getValue('instagram_default_user_account'))
      ->set('enabled_providers', $enabled_providers)
      ->save();

      parent::submitForm($form, $form_state);
  }
}
