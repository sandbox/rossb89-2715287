<?php

/**
 * @file
 * Contains \Drupal\image_blocks\FlickrApi.
 *
 * Credit to https://github.com/256cats/flickrphp for the basis of this PHP class.
 */

namespace Drupal\image_blocks;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class FlickrApi {
  private $apiKey = '';

  const FLICKR_API_BASE_URL = 'https://api.flickr.com/services/rest/?';

  public function __construct($apiKey) {
    $this->apiKey = $apiKey;
  }

  public function apiCall($params) {
    $params['api_key'] = $this->apiKey;
    $params['format'] = 'json';
    $params['nojsoncallback'] = 1;

    $encoded_params = [];

    foreach ($params as $k => $v) {
      $encoded_params[] = urlencode($k) . '=' . urlencode($v);
    }

    $url = self::FLICKR_API_BASE_URL . implode('&', $encoded_params);

    $client = new Client();
    try {
      $response = $client->get($url);
      $response_object = json_decode($response->getBody(), TRUE);
      if (empty($response_object)) {
        return FALSE;
      }
    }
    catch (RequestException $e) {
      // @todo - log a notice?
      return FALSE;
    }

    return $response_object;
  }
  
}
